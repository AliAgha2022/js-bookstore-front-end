const BACKEND_URL = "https://javalin-bookstore-api.herokuapp.com";

function fetchProducts() {
  fetch(BACKEND_URL + "/products")
    .then((result) => result.json())
    .then((products) => {
      let table = document.getElementById("products-table");

      for (let i = 0; i < products.length; i++) {
        console.log(products[i]);
        let rowCount = table.rows.length;
        let row = table.insertRow(rowCount);
        var cell1 = row.insertCell(0);
        cell1.innerText = i + 1;
        var cell2 = row.insertCell(1);
        cell2.innerText = products[i].title;
        var cell3 = row.insertCell(2);
        cell3.innerText = products[i].author?.name ?? "";
        var cell4 = row.insertCell(3);
        cell4.innerText = "€ " + products[i].price + ",-";
        var cell5 = row.insertCell(4);
        cell5.innerText = products[i].isInStock ? "Yes" : "No";
      }
    });
}

function fetchCart() {
  fetch(BACKEND_URL + "/cart")
    .then((result) => result.json())
    .then((cart) => {
      console.log(cart);
    });
}
